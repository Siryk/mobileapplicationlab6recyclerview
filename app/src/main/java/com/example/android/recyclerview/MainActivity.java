package com.example.android.recyclerview;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    private final LinkedList<String> mWordList = new LinkedList<>();

    private RecyclerView mRecyclerView;
    private WordListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> addNewWord());

        initializeWordList();
        initializeRecyclerView();
    }

    private void addNewWord() {
        int wordListSize = mWordList.size();
        mWordList.addLast("+ Word " + wordListSize);
        mRecyclerView.getAdapter().notifyItemInserted(wordListSize);
        mRecyclerView.smoothScrollToPosition(wordListSize);
    }

    private void initializeWordList() {
        for (int i = 0; i < 20; i++) {
            mWordList.addLast("Word " + i);
        }
    }

    private void initializeRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerview);
        mAdapter = new WordListAdapter(this, mWordList);
        mRecyclerView.setAdapter(mAdapter);
        setSpanCountGrid(getResources().getConfiguration());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setSpanCountGrid(newConfig);
    }

    private void setSpanCountGrid(Configuration configuration) {
        int orientation = configuration.orientation;
        int spanCount = isTabletDevice(configuration) ? (orientation == Configuration.ORIENTATION_PORTRAIT ? 2 : 3)
                : (orientation == Configuration.ORIENTATION_PORTRAIT ? 1 : 2);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));
    }

    private boolean isTabletDevice(Configuration configuration) {
        return (configuration.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE
                && configuration.smallestScreenWidthDp >= 600;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}